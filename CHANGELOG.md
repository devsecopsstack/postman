# [3.5.0](https://gitlab.com/to-be-continuous/postman/compare/3.4.1...3.5.0) (2024-05-06)


### Features

* add hook scripts support ([b13b8e4](https://gitlab.com/to-be-continuous/postman/commit/b13b8e4450e048c71a79bc2ddea03e22b76cc39c))

## [3.4.1](https://gitlab.com/to-be-continuous/postman/compare/3.4.0...3.4.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([1216d61](https://gitlab.com/to-be-continuous/postman/commit/1216d61f9c0451c15b26abffe8dbeaf08058dd68))

# [3.4.0](https://gitlab.com/to-be-continuous/postman/compare/3.3.0...3.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([47c65fc](https://gitlab.com/to-be-continuous/postman/commit/47c65fc705924625de157562efbf93a76e62450b))

# [3.3.0](https://gitlab.com/to-be-continuous/postman/compare/3.2.1...3.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([f6cf59c](https://gitlab.com/to-be-continuous/postman/commit/f6cf59cf0ac3b2cd598c761a692ea686c3cda9ab))

## [3.2.1](https://gitlab.com/to-be-continuous/postman/compare/3.2.0...3.2.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([86c36a8](https://gitlab.com/to-be-continuous/postman/commit/86c36a8d2cfb739143f470b5572d9fa5b664c89d))

# [3.2.0](https://gitlab.com/to-be-continuous/postman/compare/3.1.1...3.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([8b91d2b](https://gitlab.com/to-be-continuous/postman/commit/8b91d2bd576738b528a589a0a28960e911bb0ab8))

## [3.1.1](https://gitlab.com/to-be-continuous/postman/compare/3.1.0...3.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([fc51009](https://gitlab.com/to-be-continuous/postman/commit/fc5100915a12d8b4b1a53ce7451512ee4de7f884))

# [3.1.0](https://gitlab.com/to-be-continuous/postman/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([3011514](https://gitlab.com/to-be-continuous/postman/commit/301151402d7c54320a2368d335c6d6ab7c0822fb))

# [3.0.0](https://gitlab.com/to-be-continuous/postman/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([0a18dc0](https://gitlab.com/to-be-continuous/postman/commit/0a18dc02ecb2734325742494549572060b766251))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/postman/compare/2.0.2...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([5afd0b0](https://gitlab.com/to-be-continuous/postman/commit/5afd0b02f8b8818d4418922818318ca4e0582878))

## [2.0.2](https://gitlab.com/to-be-continuous/postman/compare/2.0.1...2.0.2) (2022-04-17)


### Bug Fixes

* support spaces in collection's filename ([24ac964](https://gitlab.com/to-be-continuous/postman/commit/24ac9640b7697159e3eba1877c72520c7b46c0b8))

## [2.0.1](https://gitlab.com/to-be-continuous/postman/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([fca4c9e](https://gitlab.com/to-be-continuous/postman/commit/fca4c9e260a4c5209da2cdc8f23727eb1e3ddbd6))

## [2.0.0](https://gitlab.com/to-be-continuous/postman/compare/1.2.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([3baa190](https://gitlab.com/to-be-continuous/postman/commit/3baa190f751fa369c871b652a5eca0921daf664f))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

Signed-off-by: Cédric OLIVIER <cedric3.olivier@orange.com>

## [1.2.0](https://gitlab.com/to-be-continuous/postman/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([6c6c844](https://gitlab.com/to-be-continuous/postman/commit/6c6c844182692a8717d64fbed7e69ec73f7b62ef))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/postman/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([00e556e](https://gitlab.com/Orange-OpenSource/tbc/postman/commit/00e556e28a6cbd8fc939ed047a11539644924fda))

## 1.0.0 (2021-05-06)

### Features

* initial release ([74b00c3](https://gitlab.com/Orange-OpenSource/tbc/postman/commit/74b00c3c6a47805b785683dcf650357fe45af089))
